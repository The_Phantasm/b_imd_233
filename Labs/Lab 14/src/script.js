var state = "IDLE";
var cmd = "";

console.log("top of statemachine loop");
do{
  switch(state){
    case "IDLE":{
      if(cmd === "run")
        {
          state = "S1";
        }
      
      break;
    }
     
    case "S1":{
      if(cmd === "next"){
        state = "S2";
      }
      
      else if(cmd === "skip"){
        state = "S3";
      }
      
      else if(cmd === "prev"){
        state = "S4";
      }
      break;
    }
     
    case "S2":{
      
      if(cmd === "next"){
        state = "S3";
      }
      
      break;
    }
      
    case "S3":{
      if (cmd === "next"){
        state = "S4";
      }
      
      if (cmd === "reset"){
        state = "IDLE";
      }
      break;
    }
      
    case "S4":{
      
      
      if (cmd === "next"){
        state = "S1";
      }
      
    }
  }
  
  cmd = getUserInput();
} while (cmd !== "exit");
console.log("bottom of statemachine loop");

function getUserInput(){
  let cmd = prompt("state:" + state, "next");
  return cmd;
}