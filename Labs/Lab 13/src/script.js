var header_data = [
  "Flight #",
  "Aircraft Type",
  "Departure Airport",
  "Arrival Airport",
  "Departed",
  "Arrived",
  "Flight Duration"
];
var flight_data = [
  {
    "Flight #": "ASA1077",
    "Aircraft Type": "A319",
    "Departure Airport": "Washington Dulles Intl (KIAD)",
    "Arrival Airport": "San Francisco Intl (KSFO)",
    Departed: "Wed 07:32PM EST",
    Arrived: "Wed 10:10PM PST"
  },
  {
    "Flight #": "ASA1088",
    "Aircraft Type": "A320",
    "Departure Airport": "San Francisco Intl (KSFO)",
    "Arrival Airport": "Washington Dulles Intl (KIAD)",
    Departed: "Wed 03:58PM PST",
    Arrived: "Wed 11:28PM EST"
  },
  {
    "Flight #": "ASA1097",
    "Aircraft Type": "A320",
    "Departure Airport": "Washington Dulles Intl (KIAD)",
    "Arrival Airport": "Los Angeles Intl (KLAX)",
    Departed: "Wed 05:06PM EST",
    Arrived: "Wed 07:24PM PST"
  },
  {
    "Flight #": "ASA11",
    "Aircraft Type": "B739",
    "Departure Airport": "Newark Liberty Intl (KEWR)",
    "Arrival Airport": "Seattle-Tacoma Intl (KSEA)",
    Departed: "Wed 05:00PM EST",
    Arrived: "Wed 07:27PM PST"
  },
  {
    "Flight #": "ASA1113",
    "Aircraft Type": "A320",
    "Departure Airport": "Will Rogers World (KOKC)",
    "Arrival Airport": "Seattle-Tacoma Intl (KSEA)",
    Departed: "Wed 05:40PM CST",
    Arrived: "Wed 07:11PM PST"
  }
];

// This is the main challenge to this lab 13 (computing the date difference)
let DurationFunc = function () {
  // Temporarily return the format for a test
  return "HH:MM:SS";
  // TODO: process Arrived and Departed into correct data format
  // arr = corrected_date_format(this.Arrived)
  // dep = corrected_date_format(this.Departed)
  return FormatTimeHHMMSS(DateDiff(dep, arr));
};

// Use a loop to add the Duration internal function to each flight_data object
for (var i = 0; i < flight_data.length; i++) {
  flight_data[i].Duration = DurationFunc;
  console.log(flight_data[i]);
}

function DateDiff(date1, date2) {
  let DeltaT = 0;
  return DeltaT;
}

// DeltaT is in milliseconds
function FormatTimeHHMMSS(dT) {
  let hr = parseInt(dT / 1000 / 3600);
  let min = parseInt((dT - hr * 3600 * 1000) / 1000 / 60);
  let sec = parseInt((dT - hr * 3600 * 1000 - min * 60 * 1000) / 1000);
  // TODO to convert the three above integers into HH:MM:SS
  let strTime = "HH:MM:SS";

  return [hr, min, sec];
}

function build_html_header(ary_data) {
  let html_str = '<div class="row ">';
  for (var i = 0; i < ary_data.length; i++) {
    html_str += '<div class="col heading">' + ary_data[i] + "</div>";
  }
  html_str += "</div>";
  return html_str;
}

function build_html_flights(data) {
  let html_str = "";
  for (var i = 0; i < data.length; i++) {
    // console.log(data[i]["Flight #"]);
    html_str += '<div class="row ">';
    html_str += '<div class="col">' + data[i]["Flight #"] + "</div>";
    html_str += '<div class="col">' + data[i]["Aircraft Type"] + "</div>";
    html_str += '<div class="col">' + data[i]["Departure Airport"] + "</div>";
    html_str += '<div class="col">' + data[i]["Arrival Airport"] + "</div>";
    html_str += '<div class="col">' + data[i]["Departed"] + "</div>";
    html_str += '<div class="col">' + data[i]["Arrived"] + "</div>";
    html_str += '<div class="col">' + FormatTimeHHMMSS() + "</div>";
    html_str += "</div>";
  }

  return html_str;
}

var el = document.getElementById("flight-data");
el.innerHTML = build_html_header(header_data);
el.innerHTML += build_html_flights(flight_data);

// This is for testing only
d2 = new Date("2021-11-17 19:27:21");
d1 = new Date("2021-11-17 14:00:01");
var ans = FormatTimeHHMMSS(d2 - d1);
console.log(ans);