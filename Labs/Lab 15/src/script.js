var stocks = [];
var header = ["Name", "Market Cap", "Sales", "Profit", "Employee no."];

stocks.push({
  name: "Microsoft",
  market_cap: "$381.7 B",
  sales: "$86.8 B",
  profit: "$22.1 B",
  numEmployees: 128000
});

stocks.push({
  name: "Starbucks",
  market_cap: "$61.6 B",
  sales: "$16.4 B",
  profit: "$2.1 B",
  numEmployees: 191000
});

stocks.push({
  name: "Costco Wholesale",
  market_cap: "$60.4 B",
  sales: "$112.6 B",
  profit: "$2.1 B",
  numEmployees: 195000
});

stocks.push({
  name: "Nike",
  market_cap: "$83.1 B",
  sales: "$27.8 B",
  profit: "$2.7 B",
  numEmployees: 56500
});

stocks.push({
  name: "Nordstrom",
  market_cap: "$15.1 B",
  sales: "$12.5 B",
  profit: "$734 M",
  numEmployees: 67000
});

stocks.push({
  name: "T-Mobile US",
  market_cap: "$21.8 B",
  sales: "$29.6 B",
  profit: "$247 M",
  numEmployees: 45000
});

stocks.push({
  name: "Amazon.com",
  market_cap: "$144.3 B",
  sales: "$89.0 B",
  profit: "-$241 M",
  numEmployees: 154100
});

stocks.push({
  name: "Zillow Group",
  market_cap: "$4.3 B",
  sales: "$325.9 M",
  profit: "-$43.6 M",
  numEmployees: 1215
});

stocks.push({
  name: "Expedia",
  market_cap: "$10.8 B",
  sales: "$5.8 B",
  profit: "$398.1 M",
  numEmployees: 18210
});

stocks.push({
  name: "Cascade Microtech",
  market_cap: "$239 M",
  sales: "$136 M",
  profit: "$9.9 M",
  numEmployees: 449
});

function buildHeader() {
  var data = '<div class = "row">';

  header.forEach(function (item) {
    data += '<div class = "col">';
    data += item;
    data += "</div>";
    console.log(item);
  });

  data += "</div>";

  //console.log(data);

  //allows us to use data in other functions
  return data;
}

function dumpData(d) {
  var el = document.getElementById("demo");

  //lopps through stocks
  stocks.forEach(function (stockObj) {
    d += '<div class = "row">';
    d += '<div class = "col">';
    d += stockObj.name;
    d += "</div>";
    d += '<div class = "col">';
    d += stockObj.market_cap;
    d += "</div>";
    d += '<div class = "col">';
    d += stockObj.sales;
    d += "</div>";
    d += '<div class = "col">';
    d += stockObj.profit;
    d += "</div>";
    d += '<div class = "col">';
    d += stockObj.numEmployees;
    d += "</div>";
    d += "</div>";
  });

  el.innerHTML = d;
}

function dewIt() {
  console.log("I was clicked!");
  //brings back data so you can concatenate onto it
  dumpData(buildHeader());
}