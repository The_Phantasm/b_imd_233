//declaring the main canvas exists basically
var canvas = document.getElementById("myCanvas");
var context = canvas.getContext("2d");

//variable declarations for circle
var x = 100, y = 300, radius = 90, angle = 2 * Math.PI;
var x_1 = 500, y_1= 300, rad = 90, angle_1 = 2* Math.PI
var x_2 = 300, y_2= 100, rad_1=90, angle_2 = 2*Math.PI;
var x_3 = 300, y_3 =500, rad, angle_3= 2*Math.PI;

//creating the main lines of the grid
context.moveTo(200, 20);
context.lineTo(200, 580);
context.moveTo(400, 20);
context.lineTo(400, 580);

context.moveTo(20, 400);
context.lineTo(580, 400);

context.moveTo(20, 200);
context.lineTo(580, 200);

context.lineWidth = 5;
context.stroke();

//drawing x's
context.moveTo(10, 10);
context.lineTo(180,190);

context.moveTo(180,15);
context.lineTo(10,190);







//move pen to center coordinates and add 90 to get rid of radius line, then draw circle using arc and stroke functions
context.moveTo(x+90, y);
context.arc(x, y, radius, 0, angle);
context.lineWidth = 5;
context.stroke();

context.moveTo(x_1+90,y_1);
context.arc(x_1,y_1,rad,0,angle_1);
context.lineWidth = 5;
context.stroke();

context.moveTo(x_2+90,y_2);
context.arc(x_2,y_2,rad,0,angle_2);
context.lineWidth = 5;
context.stroke();

context.moveTo(x_3+90,y_3);
context.arc(x_3,y_3,rad,0,angle_3);
context.lineWidth = 5;
context.stroke();