var scores = [];

scores.push({
  school: "OREGON",
  conference: "6-2",
  overall: "9-2",
  last_game: "L 7-38",
  letters:"or"
});

scores.push({
  school: "OREGON STATE",
  conference: "5-3",
  overall: "7-4",
  last_game: "W 24-10",
  letters:"osu"
});

scores.push({
  school: "WASHINGTON STATE",
  conference: "5-3",
  overall: "6-5",
  last_game: "W 44-18 ARIZ",
  letters:"wsu"
});

scores.push({
  school: "CALIFORNIA",
  conference: "3-4",
  overall: "4-6",
  last_game: "W 41-11 STAN",
  letters:"cal"
});

scores.push({
  school: "WASHINGTON",
  conference: "3-5",
  overall: "4-7",
  last_game: "L 17-20 COL",
  letters:"uw"
});

scores.push({
  school: "STANFORD",
  conference: "2-7",
  overall: "3-8",
  last_game: "L 11-41 CAL",
  letters:"su"
});

// STANFORD	 STANFORD	2-7	3-8	L 11-41 CAL	VS  NOTRE DAME11/27FOX CLICK TO SHOW MORE INFORMATION

function mainFunction() {
  //collecting node list
  var nodeList = document.querySelectorAll("li.north-div");
  //console.log("Length Data:", scores.length, nodeList.length);
  nodeList.forEach(populateRow);
}


function populateRow(obj,i){
  console.log("Obj:", obj,i);
  //puts data in their places
  obj.innerHTML = buildRowHTML(scores[i]);
  //changes ids in lists items to pull css styles
  obj.id = scores[i].letters;
}

//concatenates conference data to innerHTML
function buildRowHTML(data){
  let strHTML = '<div class = "row">'
  strHTML +=' <div class="col-sm college">'+ data.school+'</div>';
  strHTML +=' <div class="col-sm college">'+ data.conference+'</div>';
  strHTML +=' <div class="col-sm college">'+ data.overall+'</div>';
  strHTML +=' <div class="col-sm college">'+ data.last_game+'</div>';
  strHTML +='</div>';
  return strHTML;
}

function debug(x){
  console.log(x.letters);
}
// RANKED3 OREGON	6-2	9-2	L 7-38 UTAH	VS  OREGON STATE11/27ESPN CLICK TO SHOW MORE INFORMATION
// OREGON STATE	 OREGON STATE	5-3	7-4	W 24-10 ASU	@  OREGON11/27ESPN CLICK TO SHOW MORE INFORMATION


// STANFORD	 STANFORD	2-7	3-8	L 11-41 CAL	VS  NOTRE DAME11/27FOX CLICK TO SHOW MORE INFORMATION