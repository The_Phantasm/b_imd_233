var Header = ["Brand", "Model", "Year", "Price"];
//creates empty array
var Cars = [];

const Car1 = {
  brand: "Tesla",
  model: "Model 3",
  year: "2015",
  price: "$30,000"
};

const Car2 = {
  brand: "Ford",
  model: "F-150",
  year: "2000",
  price: "$20,000"
};

const Car3 = {
  brand: "BMW",
  model: "x4",
  year: "2014",
  price: "$30,000"
};

const Car4 = {
  brand: "Honda",
  model: "CRV",
  year: "2012",
  price: "$30,000"
};

const Car5 = {
  brand: "Volvo",
  model: "XC40",
  year: "2021",
  price: "$34,000"
};

//pushes each part of object to array
Cars.push(Car1);
Cars.push(Car2);
Cars.push(Car3);
Cars.push(Car4);
Cars.push(Car5);


//builds grid
function buildGrid(C) {
  var data = '<div class="row">';
  data += '<div class="col-sm">';
  data += C.brand;
  data += "</div>";
  data += '<div class="col-sm">';
  data += C.model;
  data += "</div>";
  data += '<div class="col-sm">';
  data += C.year;
  data += "</div>";
  data += '<div class="col-sm">';
  data += C.price;
  data += "</div>";
  data += "</div>";

 //calls data using CSS selector
 $("div#carGrid").append(data);
}

//creates header
function buildHeader(x) {
  var data = '<div class="row">';
  data += '<div class="col-sm">';
  data += x[0];
  data += "</div>";
  data += '<div class="col-sm">';
  data += x[1];
  data += "</div>";
  data += '<div class="col-sm">';
  data += x[2];
  data += "</div>";
  data += '<div class="col-sm">';
  data += x[3];
  data += "</div>";
  data += "</div>";

  //adds new content to html
  var el = document.getElementById("header");
  el.innerHTML = data;
}

//loops through each array element
buildHeader(Header);
Cars.forEach(function(item){
  console.log(item);
  buildGrid(item);
});