$(document).ready(function () {
  $("li").css("id", "uw");

  //hovering over items changes color
  $("ul").on("mouseover", "li", function () {
    console.log("mouseOver:" + $(this).text());
    $(this).attr("id", "uw");
  });

  $("ul").on("mouseleave", "li", function () {
    console.log("mouseLeave:" + $(this).text());
    $(this).attr("id", "uw-gold");
  });

  // reset button click
  $("button").on("click", function (e) {
    $("li").empty();
  });
  
  //removes children on list when resetting
   $("button").on("click", function (e) {
    $("ul").empty();
  });
  
   // remove 1 todo item by manipulating li
  $("ul").on("click", "li", function () {
    console.log("We clicked on the li: " + $(this).text());
    $(this).remove();
  });

  // keypress
  $("input").on("keypress", function (e) {
    var state = "IDLE";
    var code = e.which;
    var char = String.fromCharCode(code);

    //press enter
    if (code == 13) {
      //gets user input
      let inputVal = $("input").val();
      //concatenating user input in list
      let insertHTML = "<li>" + inputVal + "</li>";
      console.log(
        "key:" +
          code +
          "\tchar: " +
          char +
          "\tstate:" +
          state +
          "data" +
          inputVal);
      //add  item to list
      $("ul").append(insertHTML);
      $("input").val("");
    }
  });
});