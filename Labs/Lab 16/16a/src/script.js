window.onload = function () {
  // game element initialization
  var p1Button = document.querySelector("#p1");
  //selects button nodes
  var p2Button = document.getElementById("p2");
  var resetButton = document.getElementById("reset");
  var p1Display = document.querySelector("#p1Display");
  var p2Display = document.querySelector("#p2Display");
  var numInput = document.querySelector("input");
  var winningScoreDisplay = document.querySelector("p span");
  //initial conditions
  var p1Score = 0;
  var p2Score = 0;
  var gameOver = false;
  var winningScore = 5;

  // event listeners
  p1Button.addEventListener("click", function () {
    if (!gameOver) {
      p1Score++;
      if (p1Score === winningScore) {
        p1Display.classList.add("winner");
        p1Button.classList.add("btn-success");
        p1Button.classList.remove("btn-default");
        gameOver = true;
      }
      p1Display.textContent = p1Score;
    }
  });

  p2Button.addEventListener("click", function () {
    if (!gameOver) {
      p2Score++;
      if (p2Score === winningScore) {
        p2Display.classList.add("winner");
        p2Button.classList.add("btn-success");
        p2Button.classList.remove("btn-default");
        gameOver = true;
      }
      p2Display.textContent = p2Score;
    }
  });

  resetButton.addEventListener("click", function () {
    reset();
  });

  numInput.addEventListener("change", function () {
    if(this.value < 1){
      this.value = 1;
    }
    winningScoreDisplay.textContent = this.value;
    winningScore = Number(this.value);
    reset();
  });

  // game reset action
  function reset() {
    p1Score = 0;
    p2Score = 0;
    p1Display.textContent = 0;
    p2Display.textContent = 0;
    p1Display.classList.remove("winner");
    p2Display.classList.remove("winner");
    gameOver = false;
    p1Button.classList.remove("btn-success");
    p1Button.classList.add("btn-default");
    p2Button.classList.remove("btn-success"); 
    p2Button.classList.add("btn-default");
  }
};