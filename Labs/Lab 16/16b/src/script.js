//selecting nodes
var lis = document.querySelectorAll("li");

//iterating through nodes
for (var i = 0; i < lis.length; i++) {
  console.log(lis[i]);
  lis[i].addEventListener("mouseover", function () {
    //manipulates css selectors and text for list items
    console.log("mouseover");
    this.id= "selected";
    this.textContent = 'focused';
  });

  lis[i].addEventListener("mouseout", function () {
    console.log("mouseout");
    this.id = "deselected";
    this.textContent = 'defocused';
  });

  lis[i].addEventListener("click", function () {
    console.log("clicked");
    this.textContent = 'clicked';
  });
}

console.log("Done registering events");